// JavaScript Comments
// This is a single line comment
/*
	This is a multi line comment [cmd+option+/]
*/

// Syntax and Statements

/*
	Statements in programming - are instructions that we tell the computer to perform. It usually ends with semicolon (;)

	Syntax in programming - is the set of rules that describes how statements must be constructed
*/

console.log("Hello, World");

// Variables - it is used to contain data (container that will hold specific value).
// Declaring Variable
/* Syntax
	let / const variableName;

	Constant/const Sample ; Pi
*/

let myVariable;

console.log(myVariable);

// console.log(hello);

// let hello;

// Initializing Variables
/* Syntax
	let / const variableName = value;
*/

let productName = 'desktop computer';
console.log(productName);

let	productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
const pi = 3.1416;

// Reassigning variable values
/*Syntax
	variableName = newValue;
*/
productName='Laptop';
console.log(productName)

// interest=4.489;


// Declaring a variable before initializing a value
let supplier;

supplier="John Smith Tradings";
console.log(supplier);

// Multiple variable declarations
let productCode= 'DC017', productBrand = 'Dell';
console.log	(productCode,productBrand);

// Using a variable with a reserved keyword
/*const let = "Hello";

console.log(let);*/

// Data Types
/* 
	Strings = series of characters that create a word, phrase, sentences or anything related to creating text.
*/
let country = 'Philippines';
let province = "Metro Manila";

// Concatenating Strings
// Multiple string values can be combined to create a single string using the "+" symbol

let fullAddress = province + ', ' + country; 
console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting);

// The escpate character (\) in strings in combination with other characters can produce different effects.
// "\n" refers to creating a new line in between text

let mailAddress = 'Metro Manila\n\nPhilippines';
console.log	(mailAddress)

let message = "John's employees went home early";
console.log(message);

message = 'John\'s employees went home early';
console.log(message);

// Numbers
// Integers / Whole numbers
let headcount = 26;
console.log(headcount);

// Decimal numbers
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text and numbers
console.log("Johns grade last quarter is " + grade);

// Boolean = boolean values are normally used to store values relating to the state of certain things
let isMarried = false;
let inGoodConduct = true;
console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Arrays = are a special kind of data type thats used to store multiple values
/* Syntax
	let / const arrayName = [elementA, elementB, elementC, ...];

	let grade1 = 98.7;
	let grade2 = 92.1;
*/
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// Different data types
let details = ["John" , "Smith" , 32 , true];
console.log(details);


// Objects = are another special kind of data type thats used to mimic real world objects/items.
/*Syntax
	let / const objectName = {
		propertyA: value,
		propertyB: value
	}
*/

let	person = {
	fullName: 'Juan Dela Cruz',
	age: 35,
	isMarried: false,
	contact: ["+63917 123 4567", "8123 4567"],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}
};
console.log(person);

let myGrades = {
	firstGrading: 98.7,
	secondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6
};

console.log(myGrades);

// Null = it is used to intentionally to express the absence of a value in a variable (no value)

let spouse = null;
let myString = '';
let myNumber = 0;





